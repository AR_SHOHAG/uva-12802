#include <cstdio>
#include <iostream>
using namespace std;

int palindrome(long int n)
{
    long int temp=n, rev=0, r, i, flag=0;

    while(temp>0){
        r=temp%10;
        temp=temp/10;
        rev=rev*10+r;
    }
    if(n==rev){
        for(i=2;i<=n/2;++i){
          if(n%i==0){
              flag=1;
              break;
          }
    }
        if (flag==0)
            return 1;
        else
            return -1;
    }

    else
        return -1;

    return 0;
}


int main()
{
    long int n;

    while(scanf("%ld", &n)){
        if(palindrome(n)>0){
            printf("%ld\n", 2*n);
            return 0;
        }
        else
            printf("%ld\n", 2*n);
    }

    return 0;
}
